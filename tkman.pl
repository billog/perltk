#!/usr/bin/perl

# browser for man pages
# 
# grep references to other man pages, like 'see printf(3c)'
# and tag it and bind it to a mouse press to open a new window

my $man = "/usr/bin/man";
use Env;

use warnings;
use strict;
use Tk;
sub do_man;
my $pattern = qr/\S+\s*\(\d[\w\d]*\)/o; # looks like a man reference, eg 'sprintf(3c)'

sub button_pressed {
    my $text = shift;
    my $name = shift;
    print "look up:$name\n";
    $name =~ $pattern;
    my ($page, $section) = $name =~ /(.*)\((.*)\)/; 
    print "page=$page section=$section\n";
    do_man($page, $section);
}


sub do_man {

    my $search = shift or die "Search?";
    my $section = shift;
    my $command = "$man ";

    $section =~ s/3Xt// if $section; # 3Xt section doesn't work ?!?!?!
    $command .= "$section " if $section;


    my $mw = MainWindow->new;
    my $TextFrame = $mw->Frame( -background => 'snow' );
    my $InputFrame = $mw->Frame( -background => 'snow' );
    my $b = $InputFrame->Button( -text => "dismiss" , -command => sub {$mw->destroy()} );
    my $input = $InputFrame->Entry();
    my $grep = $InputFrame->Entry();

    my $buffer = $TextFrame->Scrolled( qw/Text -relief sunken
                                -selectbackground yellow
                                -selectforeground black
				-borderwidth 2 -setgrid true
				-scrollbars e
				/);

    $TextFrame->pack(qw/ -expand 1 -fill both/);
    $InputFrame->pack(qw/ -expand 0 -fill both/);
    #$TextFrame->grid(qw/ -rowspan 8 /);
    #$InputFrame->grid(qw//);
    $buffer->pack(qw/-expand 1 -fill both/ );
    $input->pack(qw/-side left/);
    $mw->bind('<Control-f>', sub {$buffer->FindPopUp()});

    $b->pack(-side => 'left');
    $grep->pack(qw/-side left/);
     $grep->bind('<Return>' => sub {$buffer->FindAll('-regex', '-nocase'
							, $grep->get());} );

    $input->bind('<Return>' => sub {do_man($input->get());} );

    print "$command\n";
    $input->delete('0', 'end');
    # $input->insert('0', $search); # put search in input area
    print  "2>&1 $command $search|col -b|";
    open MANPAGE, "2>&1 $command $search|col -b|";
    while (<MANPAGE>) {
	local $\ = "\n";

            # we have to allow for more than 1 tag on a line,
            # so take the 'pos()' of the search, insert text
            # up to that point and continue
            s/\s+\(/\(/; # TAGS CANNOT HAVE SPACES
	    my ($pos, $len, $tagname);
	    while (m/$pattern/g) {   # search for reference e.g: printf(3c)
	    #while (m/\w+ *\(.*?\)/g) {   # search for reference e.g: printf(3c)

		$tagname = $&;         
		$pos = pos($_);
		$len = length $&;

		$buffer->insert('end', substr($_, 0, $pos-$len));	# ordinary text
		$buffer->insert('end', "$tagname", $tagname);		# tag text
		$buffer->tagConfigure($tagname, qw/-foreground blue/);
		$buffer->tag('bind', $tagname, '<1>' => [ \&button_pressed, $tagname ]);
		# print "tagged $tagname\n";

		$_ = substr($_, $pos); # remove that already inserted
	    } 
	    $buffer->insert('end', $_) if $_; # any left over after search?
	}
	$buffer->configure(qw/-state disabled/); # turn off rw
}


print "ARGV = @ARGV\n";

@ARGV = ('man' ) unless @ARGV;

do_man(@ARGV);

MainLoop;
