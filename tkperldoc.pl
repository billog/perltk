#!/usr/bin/perl


# browser for perldoc pages
# 
# grep references to other pages, e.g: CGI::Carp
# and tag it and bind it to a mouse press to open a new window
#
# not intelligent but works OK in nearly all cases

my $pattern = qr/(\bperl\w\w+\b|\b(\w+::\w+(::\w+)*)+\b)|\w+\(\)/i; # looks like perldoc e:g 'perltoot' CGI::Carp
my $man = "perldoc -tT";
use Env;
# $ENV{PERLDOCPAGER} = "/usr/bin/cat"; # we don't want a pager!

use warnings;
use strict;
use Tk;
sub do_perldoc;

sub button_pressed {
    my $text = shift;
    my $name = shift;
    print "look up:$name\n";
    $name =~ $pattern;
    do_perldoc($name);
}


sub do_perldoc {

    my $message = "Hello";
    my $search = shift or die "Search?";
    my $command = "$man ";
    print "\nLook up for 'perltoc' may take a while\n\n" if (lc($search) eq 'perltoc');

    $command .= '-f ' if $search =~ s/\(\)//;
    $command .= $search;


    my $mw = MainWindow->new;
    my $F = $mw->Frame( -background => 'snow' );
    my $b = $mw->Button( -text => "dismiss" , -command => sub {$mw->destroy()} );
    my $msg = $mw->Label(  -relief  => 'sunken',
    			  -justify => 'left',
			  -textvariable  => \$message );

    my $input = $mw->Entry();
    # my $grep = $mw->Label( -text => "Control-F to search");
    my $buffer = $mw->Scrolled( qw/Text -relief sunken
				-selectbackground yellow
				-selectforeground black
				-borderwidth 2 -setgrid true
				-scrollbars e
				/);

    $mw->bind('<Control-f>', sub {$buffer->FindPopUp()});
    $buffer->bind('<KeyPress>', sub {$buffer->FindPopUp()});
    $msg->pack(qw/-expand 0  -fill x -side top/ );
    $F->pack(qw/ -expand 0 -fill x -fill y/);
    $buffer->pack(qw/-expand 1 -fill both/ );
    $input->pack(qw/-side left -fill x/);
    $b->pack(-side => 'right');
    # $grep->pack(qw/-side left/);
    $input->bind('<Return>' => sub {do_perldoc($input->get());} );
#    $grep->bind('<Return>' => sub {$buffer->FindAll('-regex', '-nocase', $grep->get());} );

    print "$command\n";
    $message = sprintf "$command";
    $input->delete('0', 'end');
    $input->insert('0', $search);
    #open MANPAGE, "2>&1 $command |col -b|";
    open MANPAGE, "2>&1 $command |";
    while (<MANPAGE>) {
	local $\ = "\n";

            # we have to allow for more than 1 tag on a line,
            # so take the 'pos()' of the search, insert text
            # up to that point and continue
	    my ($pos, $len, $tagname);
	    while (m/$pattern/og) {   # search for references

		$tagname = $&;         
		$pos = pos($_);
		$len = length $&;

		$buffer->insert('end', substr($_, 0, $pos-$len));	# ordinary text
		$buffer->insert('end', "$tagname", $tagname);		# tag text
		$buffer->tagConfigure($tagname, qw/-foreground blue/);
		$buffer->tag('bind', $tagname, '<1>' => [ \&button_pressed, $tagname ]);
		# print "tagged $tagname\n";

		$_ = substr($_, $pos); # remove that already inserted
	    } 
	    $buffer->insert('end', $_) if $_; # any left over after search?
	}
	$buffer->configure(qw/-state disabled/); # turn off rw
}


@ARGV = ('perldoc' ) unless @ARGV;

do_perldoc(@ARGV);

MainLoop;
